package com.example.demo.student;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Service
public class StudentService {

    public List<Student> getStudents(){
        return List.of(
                new Student(
                        1L,
                        "User M",
                        "user@gmail.com",
                        LocalDate.of(2002, Month.AUGUST,1)
                )
        );
    }
}
